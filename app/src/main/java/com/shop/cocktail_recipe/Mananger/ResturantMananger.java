package com.shop.cocktail_recipe.Mananger;

import android.content.Context;
import android.util.Log;

import com.shop.cocktail_recipe.Event.ResturantDataEvent;
import com.shop.cocktail_recipe.Interface.ApiUrl;
import com.shop.cocktail_recipe.Model.DegreeDataModel;
import com.shop.cocktail_recipe.Model.DrinkModel;
import com.shop.cocktail_recipe.Request.RestaurantDataRequest;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;



public class ResturantMananger implements RestaurantDataRequest.RestaurantDataDelegate {

    private static ResturantMananger sManagerInstance;
    private Context mAppContext;
    private List<DegreeDataModel> allVouncherList = new ArrayList<>();
    public static String role = null;



    private ResturantMananger(Context context) {
        mAppContext = context;
    }

    public static ResturantMananger getInstance() {
        return sManagerInstance;
    }

    public static ResturantMananger getInstance(Context context) {
        if (sManagerInstance == null) {
            sManagerInstance = new ResturantMananger( context );
        }
        return sManagerInstance;
    }

    public List<DegreeDataModel> getAllVouncherList() {

        List<DegreeDataModel> kk = new ArrayList<>();

        if (allVouncherList == null) {
            allVouncherList = new ArrayList<>();
        }

        return allVouncherList;
    }

    public void fetchAllVouncherList(String rolee) {
        role = rolee;
        RestaurantDataRequest showVouncherRequest = new RestaurantDataRequest( this );
        showVouncherRequest.start( ApiUrl.getBaseUrl() );

    }

    @Override
    public void restaurntDataSuccess(List<DegreeDataModel> drinkModels) {
        allVouncherList.clear();
        allVouncherList.addAll(drinkModels);
        Log.d( "MANAGER", String.valueOf(drinkModels) );
        EventBus.getDefault().post( new ResturantDataEvent(drinkModels) );
    }


}
