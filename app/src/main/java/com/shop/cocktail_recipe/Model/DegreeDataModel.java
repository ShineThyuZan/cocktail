package com.shop.cocktail_recipe.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DegreeDataModel implements Parcelable {

    @SerializedName("strDrink")
    private String strDrink;

    @SerializedName("strDrinkThumb")
    private String strDrinkThumb;

    @SerializedName("idDrink")
    private String idDrink;

    @SerializedName("role")
    private String role;


    public DegreeDataModel(String strDrink, String strDrinkThumb, String idDrink, String role) {
        this.strDrink = strDrink;
        this.strDrinkThumb = strDrinkThumb;
        this.idDrink = idDrink;
        this.role = role;
    }

    public DegreeDataModel(String role) {
        this.role = role;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(strDrink);
        dest.writeString(strDrinkThumb);
        dest.writeString(idDrink);
    }

    public DegreeDataModel(Parcel in) {
        setIdDrink(in.readString());
        setStrDrink(in.readString());
        setStrDrinkThumb(in.readString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DegreeDataModel> CREATOR = new Creator<DegreeDataModel>() {
        @Override
        public DegreeDataModel createFromParcel(Parcel in) {
            return new DegreeDataModel(in);
        }

        @Override
        public DegreeDataModel[] newArray(int size) {
            return new DegreeDataModel[size];
        }
    };

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStrDrink() {
        return strDrink;
    }

    public void setStrDrink(String strDrink) {
        this.strDrink = strDrink;
    }

    public String getStrDrinkThumb() {
        return strDrinkThumb;
    }

    public void setStrDrinkThumb(String strDrinkThumb) {
        this.strDrinkThumb = strDrinkThumb;
    }

    public String getIdDrink() {
        return idDrink;
    }

    public void setIdDrink(String idDrink) {
        this.idDrink = idDrink;
    }
}
