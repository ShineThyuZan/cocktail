package com.shop.cocktail_recipe.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class DrinkModel implements Parcelable {

    @SerializedName("drinks")
    private DegreeDataModel[] drinks;


    public static final Creator<DrinkModel> CREATOR = new Creator<DrinkModel>() {
        @Override
        public DrinkModel createFromParcel(Parcel in) {
            return new DrinkModel(in);
        }

        @Override
        public DrinkModel[] newArray(int size) {
            return new DrinkModel[size];
        }
    };

    public DegreeDataModel[] getDrinks() {
        return drinks;
    }

    public void setDrinks(DegreeDataModel[] drinks) {
        this.drinks = drinks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {

        dest.writeTypedArray(drinks, 0);

    }

    public DrinkModel(Parcel source) {
        drinks = source.createTypedArray(DegreeDataModel.CREATOR);
        setDrinks(drinks);

    }


}
