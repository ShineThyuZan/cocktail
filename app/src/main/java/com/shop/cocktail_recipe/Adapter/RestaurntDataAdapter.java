package com.shop.cocktail_recipe.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.shop.cocktail_recipe.Model.DegreeDataModel;
import com.shop.cocktail_recipe.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class RestaurntDataAdapter extends RecyclerView.Adapter<RestaurntDataAdapter.MyViewHolder> {
    Context context;
    List<DegreeDataModel> restaurantDataModelList;


    public RestaurntDataAdapter(Context context, List<DegreeDataModel> restaurantDataModels) {
        this.context = context;
        this.restaurantDataModelList = restaurantDataModels;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_data_card_view, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final DegreeDataModel dataModel = restaurantDataModelList.get(position);

        Picasso.with(context)
                .load(dataModel.getStrDrinkThumb())
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher)
                .into(holder.orderImage);
        holder.jobnumber.setText(dataModel.getStrDrink());
        holder.role.setText(dataModel.getIdDrink());
        holder.cocktailId.setText(dataModel.getRole());


    }

    @Override
    public int getItemCount() {
        return restaurantDataModelList.size();
    }

    public void setFilter(ArrayList<DegreeDataModel> list) {
        restaurantDataModelList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView jobnumber, role, cocktailId;
        ImageView orderImage;
        CardView cardView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            jobnumber = itemView.findViewById(R.id.jobno);
            orderImage = itemView.findViewById(R.id.photo);
            role = itemView.findViewById(R.id.address);
            cocktailId = itemView.findViewById(R.id.company);
            cardView = itemView.findViewById(R.id.cv_req);


        }
    }
}
