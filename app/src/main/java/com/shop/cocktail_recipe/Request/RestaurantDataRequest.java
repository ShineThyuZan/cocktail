package com.shop.cocktail_recipe.Request;

import android.util.Log;

import com.shop.cocktail_recipe.Interface.CocktailDataInterface;


import com.shop.cocktail_recipe.Mananger.ResturantMananger;
import com.shop.cocktail_recipe.Model.DegreeDataModel;
import com.shop.cocktail_recipe.Model.DrinkModel;
import com.shop.cocktail_recipe.Network.RequestBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RestaurantDataRequest implements Callback<List<DegreeDataModel>> {
    String role = ResturantMananger.role;


  private RestaurantDataRequest.RestaurantDataDelegate mDelegate;
    CocktailDataInterface cocktailDataInterface;

    public RestaurantDataRequest(RestaurantDataRequest.RestaurantDataDelegate delegate) {
      mDelegate = delegate;
    }


    public void start(String baseUrl) {
        cocktailDataInterface = RequestBuilder.createService(CocktailDataInterface.class, baseUrl);
        DegreeDataModel signInModelJustId = new DegreeDataModel(role);
        Call<List<DegreeDataModel>> call = cocktailDataInterface.getAdminLogin(signInModelJustId);
        Log.d("calllll", String.valueOf(cocktailDataInterface.getAdminLogin(signInModelJustId)));
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<DegreeDataModel>> call, Response<List<DegreeDataModel>> response) {
        List<DegreeDataModel> drinkModels = response.body();


        Log.d("response1111 ", String.valueOf(drinkModels));
        if (response.body()!=null) {
            mDelegate.restaurntDataSuccess(drinkModels);

        }
    }

    @Override
    public void onFailure(Call<List<DegreeDataModel>> call, Throwable t) {
        Log.d("fail", String.valueOf(t.getLocalizedMessage()));

    }

    public interface RestaurantDataDelegate {
        void restaurntDataSuccess(List<DegreeDataModel> drinkModels);
    }
}
