package com.shop.cocktail_recipe.Preference;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by smart04 on 6/12/17.
 */

public class SharedPreferencesManager
{
    private static SharedPreferencesManager instance = null;

    public static SharedPreferencesManager getInstance()
    {
        if(instance == null)
            instance = new SharedPreferencesManager();
        return instance;
    }

    private static final String DEFAULT_PREFERENCE = "_default_preference";
    private Map<String, SharedPreferencesModule> mSharedPreferencesModules;

    private SharedPreferencesManager()
    {
        mSharedPreferencesModules = new HashMap<>();
    }

    /**
     * get a shared preference module based on its name. creates it if needed
     * @param name shared preference module name
     * @return the {@link SharedPreferencesModule} used to write items to
     */
    public SharedPreferencesModule createSharedPreferenceModule(Context context, String name)
    {
        SharedPreferencesModule sharedPreferencesModule = new SharedPreferencesModule(context, name);
        mSharedPreferencesModules.put(name, sharedPreferencesModule);
        return sharedPreferencesModule;
    }
    /**
     * get a shared preference default module. creates it if needed
     * @return the default {@link SharedPreferencesModule} used to write items to
     */
    public SharedPreferencesModule createSharedPreferenceModule(Context context)
    {
        SharedPreferencesModule sharedPreferencesModule = new SharedPreferencesModule(context);
        mSharedPreferencesModules.put(DEFAULT_PREFERENCE, sharedPreferencesModule);
        return sharedPreferencesModule;
    }

    /**
     * retrieves a preference module if it exists
     * @param name the module name to retrieve
     * @return {@link SharedPreferencesModule} instance or null if none found
     */
    public SharedPreferencesModule getSharedPreferenceModule(String name)
    {
        return mSharedPreferencesModules.get(name);
    }

    /**
     * retrieves the default preference module if it exists
     * @return {@link SharedPreferencesModule} default instance or null if none found
     */
    public SharedPreferencesModule getSharedPreferenceModule()
    {
        return mSharedPreferencesModules.get(DEFAULT_PREFERENCE);
    }

    /**
     * checks if a module created
     * @param name the module name to check
     * @return {@code true} if module created, {@code false} otherwise
     */
    public boolean hasSharedPreferencesModule(String name)
    {
        return mSharedPreferencesModules.containsKey(name);
    }
}
