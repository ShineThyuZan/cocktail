package com.shop.cocktail_recipe;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.shop.cocktail_recipe.Activity.GetRestaurantLocationDataActivity;
import com.shop.cocktail_recipe.Adapter.RestaurntDataAdapter;
import com.shop.cocktail_recipe.AppConfig.AppConfig;
import com.shop.cocktail_recipe.Event.ResturantDataEvent;
import com.shop.cocktail_recipe.Mananger.ResturantMananger;
import com.shop.cocktail_recipe.Model.DegreeDataModel;
import com.shop.cocktail_recipe.Model.DrinkModel;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity {

    private Context mAppContext;


    RecyclerView resaurantDataRecycler;
    RestaurntDataAdapter restaurntDataAdapter;
    public static List<DrinkModel> drinkModels;
    public static List<DegreeDataModel> degreeDataModels;
    Button button;
    String role = "noAlcohol";
    EditText search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resaurantDataRecycler = findViewById(R.id.member_recycler);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        mAppContext = getApplicationContext();
        PMAppSession.getInstance(mAppContext, new AppConfig(mAppContext));
        ResturantMananger.getInstance().fetchAllVouncherList(role);

        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();

        search = findViewById(R.id.search);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchByMechanicName(editable.toString());
            }
        });
    }


    private void searchByMechanicName(String s) {
        ArrayList<DegreeDataModel> list = new ArrayList<>();
        for (DegreeDataModel otherModel : degreeDataModels) {
            if (otherModel.getStrDrink().toLowerCase().contains(s.toLowerCase())) {
                list.add(otherModel);
            }
        }
        restaurntDataAdapter.setFilter(list);
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onEvent(ResturantDataEvent showVouncherReceivedEvent) {
        degreeDataModels= showVouncherReceivedEvent.getDrinkModelList();
        // lottieAnimationView.setVisibility(View.GONE);


        resaurantDataRecycler.setHasFixedSize(true);
        resaurantDataRecycler.setLayoutManager(new LinearLayoutManager(getApplication()));
        restaurntDataAdapter = new RestaurntDataAdapter(getApplication(), degreeDataModels);
        resaurantDataRecycler.setAdapter(restaurntDataAdapter);

    }
}
