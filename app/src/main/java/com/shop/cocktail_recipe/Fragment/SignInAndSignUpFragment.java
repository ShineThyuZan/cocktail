package com.shop.cocktail_recipe.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.shop.cocktail_recipe.R;

import de.greenrobot.event.EventBus;

import static com.shop.cocktail_recipe.Activity.SplashActivity.LOGGED_IN_PREF;


public class SignInAndSignUpFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    EditText userNameEdt;
    EditText passwordEdt;
    Button signUpBtn;
    Button signInBtn;
    TextView forgetPasswordTv;
    TextView message;
    String userNameStr;
    String passwordStr;
    public static boolean Check;
    SharedPreferences sharedPrefs;
    ProgressDialog progressDialog;

    public static SignInAndSignUpFragment newInstance(String param1, String param2) {
        SignInAndSignUpFragment fragment = new SignInAndSignUpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sign_in_and_sign_up, container, false);
        userNameEdt = v.findViewById(R.id.user_input_name);
        passwordEdt = v.findViewById(R.id.password);
        signInBtn = v.findViewById(R.id.signIn);
        signUpBtn = v.findViewById(R.id.signUp);
        forgetPasswordTv = v.findViewById(R.id.forget_Pw);
        message = v.findViewById(R.id.errormsg);
        sharedPrefs = getActivity().getSharedPreferences(LOGGED_IN_PREF, Context.MODE_PRIVATE);

        return v;

    }
}
