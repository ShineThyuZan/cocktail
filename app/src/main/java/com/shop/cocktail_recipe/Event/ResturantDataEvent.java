package com.shop.cocktail_recipe.Event;


import com.shop.cocktail_recipe.Model.DegreeDataModel;
import com.shop.cocktail_recipe.Model.DrinkModel;

import java.util.ArrayList;
import java.util.List;

public class ResturantDataEvent extends ArrayList<DegreeDataModel> {

    List<DegreeDataModel> drinkModelList;


    public ResturantDataEvent(List<DegreeDataModel> drinkModelList) {
        this.drinkModelList = drinkModelList;
    }

    public List<DegreeDataModel> getDrinkModelList() {
        return drinkModelList;
    }

    public void setDrinkModelList(List<DegreeDataModel> drinkModelList) {
        this.drinkModelList = drinkModelList;
    }
}
