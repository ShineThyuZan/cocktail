package com.shop.cocktail_recipe.Interface;

import com.shop.cocktail_recipe.Model.DegreeDataModel;
import com.shop.cocktail_recipe.Model.DrinkModel;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface CocktailDataInterface {
    @Headers("Content-Type: application/json")
    @POST("/getCocktailData")
    Call<List<DegreeDataModel>> getAdminLogin(@Body DegreeDataModel role);

}
