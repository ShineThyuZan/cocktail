package com.shop.cocktail_recipe.Activity;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.shop.cocktail_recipe.Adapter.Item;
import com.shop.cocktail_recipe.Adapter.RestaurntDataAdapter;
import com.shop.cocktail_recipe.Event.ResturantDataEvent;
import com.shop.cocktail_recipe.Mananger.ResturantMananger;
import com.shop.cocktail_recipe.Model.DegreeDataModel;
import com.shop.cocktail_recipe.Model.DrinkModel;
import com.shop.cocktail_recipe.R;
import java.util.ArrayList;
import java.util.List;
import de.greenrobot.event.EventBus;

public class GetRestaurantLocationDataActivity extends AppCompatActivity {

    TextView companyAddress;
    RecyclerView resaurantDataRecycler;
    RestaurntDataAdapter restaurntDataAdapter;
    Dialog progressDialog;
    ArrayList<DrinkModel> allData = new ArrayList<>();
    String role = "noAlcohol";
    DrinkModel drinkModels;
    List<DegreeDataModel> degreeDataModels;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_restaurnt_location_data);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        resaurantDataRecycler = findViewById(R.id.member_recycler);
        button = findViewById(R.id.search);

        ResturantMananger.getInstance().fetchAllVouncherList(role);
        showDialog();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onEvent(ResturantDataEvent showVouncherReceivedEvent) {
        degreeDataModels= showVouncherReceivedEvent.getDrinkModelList();
       // lottieAnimationView.setVisibility(View.GONE);


        resaurantDataRecycler.setHasFixedSize(true);
        resaurantDataRecycler.setLayoutManager(new LinearLayoutManager(getApplication()));
        restaurntDataAdapter = new RestaurntDataAdapter(getApplication(), degreeDataModels);
        resaurantDataRecycler.setAdapter(restaurntDataAdapter);

    }




    private void showDialog() {

        progressDialog = new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //progressDialog.setContentView( R.layout.loading_dialog );
        progressDialog.setCancelable(false);
        progressDialog.show();


    }


}