package com.shop.cocktail_recipe.Activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.shop.cocktail_recipe.R;

public class RestLocationDetailActivity extends AppCompatActivity {
    TextView textViewLati;
    TextView textViewLongi;
    Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_rest_location_detail );


        //  Toolbar toolbar = findViewById( R.id.toolbar );
        textViewLati = findViewById( R.id.lati );
        textViewLongi = findViewById( R.id.longi );
        Bundle bundle = getIntent().getExtras();
        String latti = bundle.getString( "latti" );
        String longi = bundle.getString( "longi" );


        textViewLati.setText( latti );
        textViewLongi.setText( longi );

        FloatingActionButton fab = findViewById( R.id.fab );
        fab.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make( view, "Replace with your own action", Snackbar.LENGTH_LONG )
                        .setAction( "Action", null ).show();
            }
        } );
    }




}
