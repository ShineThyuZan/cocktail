package com.shop.cocktail_recipe.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.shop.cocktail_recipe.MainActivity;
import com.shop.cocktail_recipe.R;


public class SplashActivity extends Activity {

    TextView tvUMG;
    public static final String LOGGED_IN_PREF = "logged_in_status";
    public static String customerIdStr;
    public static String customerName;
    public static Boolean isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        SharedPreferences pref = this.getSharedPreferences(LOGGED_IN_PREF, Context.MODE_PRIVATE);
        isLogin = pref.getBoolean("islogin", false);
        customerIdStr = pref.getString("buyerid", "");
        customerName = pref.getString("fname", "") + pref.getString("lname", "");
        Log.d("splash", customerIdStr);
        Log.d("splash", String.valueOf(isLogin));


        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(2 * 1000);
                    Intent i = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                } catch (Exception e) {
                } finally {
                    if (isLogin == true) {

//                        AccountFragment.Check = Boolean.valueOf(SharepreferenceLoginCode.readSharedSetting(getApplicationContext(), "ClipCodes", true));
//                        Intent intent = new Intent(SplashActivity.this,MainActivity.class);
//                        startActivity(intent);
                    } else if (isLogin == false) {
//                        AccountFragment.Check = Boolean.valueOf(SharepreferenceLoginCode.readSharedSetting(getApplicationContext(), "ClipCodes", false));
//                        SharepreferenceLoginCode.saveSharedSetting(getApplicationContext(), "ClipCodes", true);
                    }
                }
            }
        };

        background.start();
    }
}